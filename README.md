# nventry
nventry is a container entrypoint designed to handle common NGC container entrypoint needs.

# Use in Dockerfile
```
COPY --from=registry.gitlab.com/nvhpc/nventry:v0.7.1 /nventry /usr/bin/nventry
```

# Native Installation
```
$ cd $GOPATH
$ git clone https://gitlab.com/nvhpc/nventry.git
$ cd nventry
$ go build nventry
```

## Building the example image
```
$ cd example
$ cp ../nventry .
$ hpccm --format docker --recipe recipe.py > Dockerfile
$ docker build -t nventry:example .
```

# Enable
To enable nventry set it as the entrypoint of the container image.
```
ENTRYPOINT ["/path/to/nventry", "<options>"]
```

Flags may also be set by the user at runtime, adding additional functionality or overriding flags set during container creation.

```
$ docker run -it --rm foo:latest -gpu_check=false foo.exe
```

# Options
```
$ ./nventry --help
Usage of ./nventry:
  -entrypoint string
    	Path to entrypoint which will be called after nventry has prepared the environment
  -gpu_base_dir string
    	Set the base directory of optimized binaries/libraries
  -gpu_compat_driver string
    	compatibility gpu driver supported, e.g. 384.0
  -gpu_default_driver string
    	default gpu driver required, e.g. 384.111
  -gpu_disable_check
    	If set to true disable all GPU checks
  -ignore_fatal
    	Ignore fatal configuration errors and proceed with execution
  -ofed_base_dir string
    	path to all (M)OFED installations
  -ofed_disable_check
    	If set to true disable all OFED checks
  -ofed_force_version string
    	For the specified OFED version in the container. May be set through environment variable NVEP_OFED_FORCE_VERSION
  -quiet
    	disable most logging
  -ucx_base_dir string
    	path to all UCX installations
  -ucx_disable_check
    	If set to true disable all UCX checks
  -verbose
    	enable verbose logging
```

# Use Cases
The following examples demonstrate common use cases for `nventry`

## GPU architecture support
It is sometimes necessary to build multiple application binaries, each targeting a particular GPU architecture. In this case the following flags
may be used.
```
nventry -gpu_base_dir=/opt/app
```

`gpu_base_dir` should point to a directory with the following hierarchy
```
|-- /opt/app
|   |-- sm60
|       |-- bin
|       |-- lib
|    |-- sm70
|       |-- bin
|       |-- lib
```

If set the host GPU architecture will be queried, after which the `PATH` and `LD_LIBRARY_PATH` will be set appropriately based upon the directory structure above.

## GPU driver verification
Non `nvidia-docker` container runtimes do not typically check if the GPU driver is capable of supporting the CUDA runtime installed within the container. Some 
applications additionally have driver requirements that are more strict than those of the runtime itself. This check will be enabled automatically if possible.

## GPU driver compatibility mode
GPU compatability mode will be enabled automatically if detected.

## OFED version selection
Creating a portable HPC container often requires multiple Infiniband user space driver components be packaged in a single container. When enabled `nventry` will query the host IB driver and match it with an appropriate set of user space libraries.

```
nventry -ofed_base_dir=/opt/ofed
```
`ofed_base_dir` should point to a directory with the following hierarchy
```
|-- /opt/ofed
|   |-- 4.4-1.0.0
|       |-- bin
|       |-- lib
|           |-- libibverbs
|    |-- 5.0-0
|       |-- bin
|       |-- lib
|           |-- libibverbs
```
libibverbs should contain the list provider drivers, e.g. `libmlx5-rdmav2.so`. These libraries will be made available through `IBV_DRIVERS` and SHOULD NOT be provided through the system wide `/etc/libibverbs.d` directory.

## UCX flavor selection
UCX may be built against the RMDA-core or Mellanox IB libraries. At runtime the correct UCX must be made available based upon the OFED version in use.
```
nventry -ucx_base_dir=/opt/ucx
```

`ucx_base_dir` should point to a directory with the following hierarchy
```
|-- /opt/ucx
|   |-- mofed
|       |-- bin
|       |-- lib
|    |-- rdmacore
|       |-- bin
|       |-- lib
```
If set the OFED version will be used to determine the best UCX installation to make available by setting `PATH` and `LD_LIBRARY_PATH` based upon the directory structure above. Determining if the OFED version is `mofed` or `rdmacore` is the only selection criteria.


# Singularity
When running under singularity the `run` command(opposed to `exec` and `shell`) must be used to enable the entrypoint:
```
$ singularity run foo.simg
```
By default when no command is provided `/bin/bash` will be run, mimicking Docker behavior
